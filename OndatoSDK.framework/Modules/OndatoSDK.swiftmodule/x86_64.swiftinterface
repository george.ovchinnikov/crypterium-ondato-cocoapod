// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name OndatoSDK
import AVFoundation
import AVKit
import CoreGraphics
import FaceTecSDK
import Foundation
import ImageIO
import MobileCoreServices
@_exported import OndatoSDK
import Photos
import ReplayKit
import Swift
import UIKit
import _Concurrency
@_hasMissingDesignatedInitializers @objc(OndatoServiceError) public class OndatoServiceError : ObjectiveC.NSObject {
  @objc(OndatoServiceErrorType) public enum OndatoServiceErrorType : Swift.Int {
    case cancelled
    case invalidServerResponse
    case invalidCredentials
    case recorderPermissions
    case unexpectedInternalError
    case verificationFailed
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  final public let type: OndatoSDK.OndatoServiceError.OndatoServiceErrorType
  final public let error: Swift.String?
  final public let message: Swift.String?
  @objc deinit
}
@objc(OndatoLocalizationBundle) public class OndatoLocalizationBundle : ObjectiveC.NSObject {
  @objc public static func bundle(with bundle: Foundation.Bundle, tableName: Swift.String) -> OndatoSDK.OndatoLocalizationBundle
  @objc public init(bundle: Foundation.Bundle, tableName: Swift.String)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc(Ondato) public class Ondato : ObjectiveC.NSObject {
  @objc public static var sdk: OndatoSDK.Ondato
  @objc weak public var delegate: OndatoSDK.OndatoFlowDelegate?
  @objc public var configuration: OndatoSDK.OndatoServiceConfiguration
  @objc public var identificationId: Swift.String {
    @objc get
    @objc set(value)
  }
  @objc public func initialize(username: Swift.String, password: Swift.String)
  @objc public func initialize(accessToken: Swift.String)
  @objc public func instantiateOndatoViewController() -> OndatoSDK.OndatoMainViewController
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(OndatoAppearance) public class OndatoAppearance : ObjectiveC.NSObject {
  @objc public var consentWindow: OndatoSDK.OndatoConsentAppearance
  @objc public var progressColor: UIKit.UIColor
  @objc public var errorColor: UIKit.UIColor
  @objc public var errorTextColor: UIKit.UIColor
  @objc public var buttonColor: UIKit.UIColor
  @objc public var buttonTextColor: UIKit.UIColor
  @objc public var textColor: UIKit.UIColor
  @objc public var backgroundColor: UIKit.UIColor
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(OndatoGDRPAppearance) public class OndatoConsentAppearance : ObjectiveC.NSObject {
  @objc public var header: OndatoSDK.OndatoLabelAppearance
  @objc public var body: OndatoSDK.OndatoTextViewAppearance
  @objc public var acceptButton: OndatoSDK.OndatoButtonAppearance
  @objc public var declineButton: OndatoSDK.OndatoButtonAppearance
  @objc override dynamic public init()
  @objc deinit
}
@objc(OndatoButtonAppearance) public class OndatoButtonAppearance : ObjectiveC.NSObject {
  @objc public var font: UIKit.UIFont
  @objc public var backgroundColor: UIKit.UIColor
  @objc public var tintColor: UIKit.UIColor
  @objc public var borderWidth: CoreGraphics.CGFloat
  @objc public var borderColor: UIKit.UIColor
  @objc public var cornerRadius: CoreGraphics.CGFloat
  public init(font: UIKit.UIFont = .systemFont(ofSize: 15), backgroundColor: UIKit.UIColor = .clear, tintColor: UIKit.UIColor = .systemBlue, borderWidth: CoreGraphics.CGFloat = 0, borderColor: UIKit.UIColor = .clear, cornerRadius: CoreGraphics.CGFloat = 0)
  @objc deinit
}
@objc(OndatoLabelAppearance) public class OndatoLabelAppearance : ObjectiveC.NSObject {
  @objc public var font: UIKit.UIFont
  @objc public var color: UIKit.UIColor
  public init(font: UIKit.UIFont = .systemFont(ofSize: 15, weight: .semibold), color: UIKit.UIColor = .black)
  @objc deinit
}
@objc(OndatoTextViewAppearance) public class OndatoTextViewAppearance : ObjectiveC.NSObject {
  @objc public var font: UIKit.UIFont
  @objc public var textColor: UIKit.UIColor
  public init(font: UIKit.UIFont = .systemFont(ofSize: 15), textColor: UIKit.UIColor = .black)
  @objc deinit
}
@objc(OndatoSupportedLanguage) public enum OndatoSupportedLanguage : Swift.Int, Swift.RawRepresentable {
  case DE
  case EN
  case ET
  case LT
  case LV
  case RU
  case SQ
  public typealias RawValue = Swift.String
  public var rawValue: OndatoSDK.OndatoSupportedLanguage.RawValue {
    get
  }
  public var displayName: Swift.String {
    get
  }
  public init?(rawValue: OndatoSDK.OndatoSupportedLanguage.RawValue)
}
extension OndatoSDK.OndatoSupportedLanguage : Swift.CaseIterable {
  public typealias AllCases = [OndatoSDK.OndatoSupportedLanguage]
  public static var allCases: [OndatoSDK.OndatoSupportedLanguage] {
    get
  }
}
@_hasMissingDesignatedInitializers @objc @_Concurrency.MainActor(unsafe) public class OndatoMainViewController : UIKit.UINavigationController {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidAppear(_ animated: Swift.Bool)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers public class OndatoLog : ObjectiveC.NSObject {
  public static var shared: OndatoSDK.OndatoLog
  public var logs: [Swift.String] {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
@objc(OndatoServerMode) public enum OndatoEnvironment : Swift.Int {
  case test = 0
  case live = 1
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension UIKit.UIDevice {
  @_Concurrency.MainActor(unsafe) public static var modelName: Swift.String
}
extension UIKit.UIImage {
  public class func gifImageWithData(_ data: Foundation.Data) -> UIKit.UIImage?
  public class func gifImageWithURL(_ gifUrl: Swift.String) -> UIKit.UIImage?
  public class func gifImageWithName(_ name: Swift.String) -> UIKit.UIImage?
}
@objc(OndatoFlowDelegate) public protocol OndatoFlowDelegate {
  @objc optional func viewControllerForStart(startPressed: @escaping () -> Swift.Void) -> UIKit.UIViewController
  @objc optional func viewForLoading(progress: Swift.Float) -> UIKit.UIView?
  @objc optional func viewForSuccess(continue: @escaping () -> Swift.Void) -> UIKit.UIView
  @objc func flowDidSucceed(identificationId: Swift.String?)
  @objc func flowDidFail(identificationId: Swift.String?, error: OndatoSDK.OndatoServiceError)
}
@objc(OndatoLivenessMode) public enum OndatoLivenessMode : Swift.Int {
  case active
  case passive
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc(OndatoServiceConfiguration) public class OndatoServiceConfiguration : ObjectiveC.NSObject {
  @objc public var appearance: OndatoSDK.OndatoAppearance
  @objc public var flowConfiguration: OndatoSDK.OndatoFlowConfiguration
  @objc public var recorderConfiguration: OndatoSDK.OndatoRecorderConfiguration
  @objc public var mode: OndatoSDK.OndatoEnvironment
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(OndatoFlowConfiguration) public class OndatoFlowConfiguration : ObjectiveC.NSObject {
  @objc public var showSplashScreen: Swift.Bool
  @objc public var showStartScreen: Swift.Bool
  @objc public var showConsentScreen: Swift.Bool
  @objc public var showSelfieWithDocumentScreen: Swift.Bool
  @objc public var showSuccessWindow: Swift.Bool
  @objc public var livenessCheck: OndatoSDK.OndatoLivenessMode
  @objc public var driversLicenceBacksideRequired: Swift.Bool
  @objc public var removeSelfieFrame: Swift.Bool
  @objc public var recordProcess: Swift.Bool
  @objc public var waitForResult: Swift.Bool
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(OndatoRecorderConfiguration) public class OndatoRecorderConfiguration : ObjectiveC.NSObject {
  @objc public var bitrate: Swift.Int
  @objc public var resolutionRatio: Swift.Float
  @objc override dynamic public init()
  @objc deinit
}
@objc(OndatoDocumentType) public enum OndatoDocumentType : Swift.Int, Swift.Codable {
  case passport = 0
  case idCard = 1
  case drivingLicence = 2
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc(OndatoLocalizeHelper) public class OndatoLocalizeHelper : ObjectiveC.NSObject {
  @objc public static var language: OndatoSDK.OndatoSupportedLanguage
  @objc public static func setLocalizationBundle(_ bundle: OndatoSDK.OndatoLocalizationBundle, for language: OndatoSDK.OndatoSupportedLanguage)
  @objc deinit
}
