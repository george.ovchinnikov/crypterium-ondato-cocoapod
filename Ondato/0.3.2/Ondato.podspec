Pod::Spec.new do |s|
    s.name              = 'Ondato'
    s.version           = '0.3.2'
    s.summary           = 'Crypterium Ondato integration'
    s.homepage          = 'https://gitlab.com/george.ovchinnikov/crypterium-ondato-cocoapod'

    s.author            = { 'Georgiy Ovchinnikov' => 'georgiy.ovchinnikov@crypterium.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://gitlab.com/george.ovchinnikov/crypterium-ondato-cocoapod.git',
                            :tag => s.version }

    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    s.swift_versions = ['5.3']

    s.ios.deployment_target = '11.0'
    s.ios.vendored_frameworks =
    'OndatoSDK.framework',
    'FaceTecSDK.framework'

end
